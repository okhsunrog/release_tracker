# release_tracker

Helps you to keep track of your favorite repos. 
Now you will never miss a new release of your favorite FOSS app! 

Supports GitHub, GitLab and codeberg.org. 

How to use:

1. Install deps for the script (look in "imports" section)

2. Create a file `config.ini` with content like this:
```
[Tokens]
github_token = your_token
gitlab_token = your_token

[DB]
path = mydatabase.db
```
You need to specify your keys and a path to your db file.
If db path is not specified `template_database.db` will be used.

_Note: You may be wondering why you don't need a key for Codeberg. Codeberg doesn't have such kind of API for now, so the script just parses the releases webpage using BeautifulSoup._

3. Open the SQLite datebase and edit `repo` table to your likings, you can leave version fields blank:
```
sqlite3 mydatabase.db
```
_Note: there's a template in my repo, you can also use it for testing_

4. Run the script and enjoy!
```
python tracker.py
```