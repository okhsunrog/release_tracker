import requests
import configparser
import sqlite3
from urllib.parse import urlparse
from urllib.parse import quote
from bs4 import BeautifulSoup
from github import Github
import sys

def codeberg_latest_release(repo_url):
    try:
        response = requests.get(repo_url + "/releases")
    except requests.exceptions.RequestException as e:
        print(f'Error: {e}')
    else:
        soup = BeautifulSoup(response.content, 'html.parser')
        selected_div = soup.find('div', class_='item tag selected')
        return selected_div.text


def get_repo_name(repo_url):
    parsed_url = urlparse(repo_url)
    return parsed_url.path[1:]


def github_latest_release(repo_url):
    g = Github(github_token)
    repo = g.get_repo(get_repo_name(repo_url))
    latest_release = repo.get_latest_release()
    return latest_release.title


def gitlab_latest_release(repo_url):
    api_base_url = "https://gitlab.com/api/v4"
    repo_path = repo_url.replace("https://gitlab.com/", "")
    encoded_repo_path = quote(repo_path, safe='')
    api_url = f"{api_base_url}/projects/{encoded_repo_path}/repository/tags"
    params = { "private_token": gitlab_token }
    response = requests.get(api_url, params=params)
    if response.status_code != 200:
        raise Exception(f"Failed to get tags for repository {repo_url}")
    tags = response.json()
    latest_tag = tags[0]["name"]
    return latest_tag


def get_service_name(url):
    parsed_url = urlparse(url)
    return parsed_url.netloc


def latest_release(repo_url):
    service_name = get_service_name(repo_url)
    if service_name == "github.com":
        return github_latest_release(repo_url)
    elif service_name == "gitlab.com":
        return gitlab_latest_release(repo_url)
    elif service_name == "codeberg.org":
        return codeberg_latest_release(repo_url)
    else:
        return "unknown service"


def verpr(text):
    if verbose_mode:
            print(text)


print("starting release tracker...")
config = configparser.ConfigParser()
config.read('config.ini')
github_token = config['Tokens']['github_token']
gitlab_token = config['Tokens']['gitlab_token']
bg_mode = 0
verbose_mode = 1
if len(sys.argv) > 1:
    if sys.argv[1] == '-b':
        bg_mode = 1
        ntfy_server = config['NTFY']['ntfy_server']
        topic = config['NTFY']['topic']
if bg_mode:
    print('bg_mode enabled')
    verbose_mode = 0
    if len(sys.argv) > 2:
        if sys.argv[2] == '-v':
            verbose_mode = 1
            print("verbose output active")
else:
    print('bg_mode disabled')
try:
    db_path = config['DB']['path']
except KeyError:
    db_path = "template_database.db"
    pass
verpr(f"Using {db_path}")
conn = sqlite3.connect(db_path)
conn.execute('CREATE TABLE IF NOT EXISTS repos (url text, version text)')
conn.commit()
cursor = conn.execute('SELECT * FROM repos ORDER BY url ASC')
repos = cursor.fetchall()
for r in repos:
    repo_url = r[0]
    repo_name = get_repo_name(repo_url)
    repo_db_ver = r[1]
    silent = r[2]
    verpr(f"cheching {repo_name}...")
    repo_cur_ver = latest_release(repo_url)
    if repo_db_ver != repo_cur_ver:
        info = f"{repo_name} Found a new verion: {repo_cur_ver}"
        verpr(info)
        if bg_mode:
            if silent != 1:
                verpr("sending ntfy")
                requests.post(f"{ntfy_server}/{topic}", data = info)
                cursor.execute(f"UPDATE repos SET silent = 1 WHERE url = '{repo_url}'")
            else:
                verpr("silent: not sending ntfy")
        else:
            print("Update db ver? (y/N)")
            if input() == "y":
                cursor.execute(f"UPDATE repos SET version = '{repo_cur_ver}' WHERE url = '{repo_url}'")
                cursor.execute(f"UPDATE repos SET silent = 0 WHERE url = '{repo_url}'")
                print("Updated db version!")
    else:
        verpr(f"Up to date: {repo_cur_ver}")
    verpr("")
conn.commit()
cursor.close()
conn.close()
